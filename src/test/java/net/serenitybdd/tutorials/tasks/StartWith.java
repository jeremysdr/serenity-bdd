package net.serenitybdd.tutorials.tasks;

import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.actions.*;
import net.thucydides.core.annotations.*;
import net.thucydides.core.pages.*;
import static net.serenitybdd.screenplay.Tasks.instrumented;


public class StartWith implements Task {

  @DefaultUrl("http://todomvc.com/examples/dojo/")
  public class TodoMvcApplicationHomePage extends PageObject {
  }

  TodoMvcApplicationHomePage todoMvcApplicationHomePage;

  @Override
  @Step("{0} starts with an empty todo list")
  public <T extends Actor> void performAs(T actor) {
    actor.attemptsTo(
      Open.browserOn().the(todoMvcApplicationHomePage)
    );
  }

  public static StartWith anEmptyTodoList() {
    return instrumented(StartWith.class);
  }
}