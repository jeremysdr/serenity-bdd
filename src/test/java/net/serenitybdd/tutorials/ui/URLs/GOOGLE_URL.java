package net.serenitybdd.tutorials.ui.URLs;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://www.google.com")
public class GOOGLE_URL extends PageObject { }